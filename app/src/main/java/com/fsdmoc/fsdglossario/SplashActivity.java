package com.fsdmoc.fsdglossario;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.fsdmoc.fsdglossario.datastore.Settings;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SPLASH_TIME_OUT = 3500;
    private Handler handler;
    private Runnable runnable;
    //private ImageView imageViewLogo;
    //private Animation rotateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //imageViewLogo = (ImageView) findViewById(R.id.imageViewLogo);
        //startRotateLogo();

        View root = findViewById(R.id.splash);
        root.setOnClickListener(this);

        Settings settings = new Settings(this);
        if (settings.isFirstAppRun()) {

            Log.d("Splash", "is first run");

            //get locale from phone and save on our preferences

            settings.saveSystemLanguage();
            Log.d("Splash", "saved system language to preferences");

            settings.applyPreferredLanguageToApp();
            Log.d("Splash", "applied preferred language to app");

            settings.saveNotFirstAppRun();
            Log.d("Splash", "saved not first run");

        } else {
            //configurar a app para usar o idioma escolhido pelo user (na nossa preferences), e nao
            //necessariamente o idioma do celular.

            Log.d("Splash", "NOT first app run");
            settings.applyPreferredLanguageToApp();
            Log.d("Splash", "applied preferred language to app");

        }

        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {

                Log.d("Splash", "launching main activity");
                //rotateAnimation.cancel();
                launchMainActivity();
                finish();
            }
        };
        handler.postDelayed(runnable, SPLASH_TIME_OUT);
    }

    private void launchMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

        //since user cliked, they are trying to be fast
        //so cancel the timer and just open the main activity
        handler.removeCallbacks(runnable);
        //rotateAnimation.cancel();
        launchMainActivity();
        finish();

    }

    /*private void startRotateLogo() {

        rotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_rotate);
        imageViewLogo.startAnimation(rotateAnimation);
    }*/

}
