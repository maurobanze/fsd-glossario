package com.fsdmoc.fsdglossario;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fsdmoc.fsdglossario.datastore.LocalDatastore;
import com.fsdmoc.fsdglossario.datastore.Settings;
import com.fsdmoc.fsdglossario.entities.Term;
import com.google.firebase.analytics.FirebaseAnalytics;

public class TermDetailsActivity extends AppCompatActivity {

    private Term term;
    private TextView textViewName, textViewDefinition, textViewAdditionalInfo,
            textViewLabelAdditionalInfo;

    private boolean isPortuguese;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_details);

        setTitle(R.string.activity_title_term_details);
        textViewName = (TextView) findViewById(R.id.textViewTermName);
        textViewDefinition = (TextView) findViewById(R.id.textViewDefinition);
        textViewAdditionalInfo = (TextView) findViewById(R.id.textViewAdditionalInfo);
        textViewLabelAdditionalInfo = (TextView) findViewById(R.id.textViewLabelAdditionalInfo);

        int termId = getIntent().getIntExtra(EXTRA_TERM_ID, -1);
        fetchTerm(termId);

        isPortuguese = Settings.ActiveLanguage.equals(Settings.LANGUAGE_PT);
        populateFields();

        addTermToRecentsQueue();
        logAnalyticsTermDisplay();
    }


    private void fetchTerm(int termId) {

        //Fetch from datastore using id.
        term = LocalDatastore.initializeOrReturn(getApplication()).getTerm(termId);

    }

    private void populateFields() {

        String name, definition, additionalInfo;

        if (isPortuguese) {

            if (isBlank(term.getName())) {
                Toast.makeText(this, R.string.toast_term_no_language, Toast.LENGTH_SHORT).show();
                return;
            }
            name = term.getName();
            definition = term.getDefinition();
            additionalInfo = term.getAdditionalInformation();

        } else {

            if (isBlank(term.getEnName())) {
                Toast.makeText(this, R.string.toast_term_no_language, Toast.LENGTH_SHORT).show();
                return;
            }
            name = term.getEnName();
            definition = term.getEnDefinition();
            additionalInfo = term.getEnAdditionalInformation();
        }

        textViewName.setText(name);
        textViewDefinition.setText(definition);


        if (isBlank(additionalInfo)) {
            textViewLabelAdditionalInfo.setVisibility(View.GONE);
            textViewAdditionalInfo.setVisibility(View.GONE);

        } else {
            textViewAdditionalInfo.setText(additionalInfo);

            textViewLabelAdditionalInfo.setVisibility(View.VISIBLE);
            textViewAdditionalInfo.setVisibility(View.VISIBLE);

        }
    }

    private void switchLanguage() {

        isPortuguese = !isPortuguese;

    }

    private void shareTerm(String textToShare) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_app_title)));
    }

    /**
     * Uses db to add the term displayed to the recents list
     */
    private void addTermToRecentsQueue() {

        //TODO. Use async task instead as this
        LocalDatastore.initializeOrReturn(getApplication())
                .addToRecents(term, Settings.ActiveLanguage);
    }

    private boolean isBlank(String string) {

        return string.isEmpty() || string.trim().isEmpty();
    }

    private void logAnalyticsTermDisplay() {

        Bundle params = new Bundle();
        params.putString("termId", term.getId() + "");
        params.putString("termLanguage", Settings.ActiveLanguage);
        FirebaseAnalytics.getInstance(this).logEvent("TERM_VIEW", params);

    }

    private void logAnalyticsShareTerm(){

        Bundle params = new Bundle();
        params.putString("termId", term.getId() + "");
        params.putString("termLanguage", Settings.ActiveLanguage);
        FirebaseAnalytics.getInstance(this).logEvent("SHARE_TERM", params);
    }

    private void logAnalyticsSwitchTermLanguage(){


        FirebaseAnalytics.getInstance(this).logEvent("SWITCH_TERM_LANGUAGE", null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_term_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_switch_language) {

            logAnalyticsSwitchTermLanguage();
            switchLanguage();
            populateFields();
            return true;

        } else if (id == R.id.action_share_term) {

            logAnalyticsShareTerm();
            shareTerm(term.getName() + ": " + term.getDefinition() + "\n\n" + getString(R.string.message_share_term));
            return true;

        } else if (id == android.R.id.home) {

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static final String EXTRA_TERM_ID = "EXTRA_TERM_ID";

    @Override
    public void onBackPressed() {

        finish();
    }


}
