package com.fsdmoc.fsdglossario;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fsdmoc.fsdglossario.datastore.Settings;
import com.fsdmoc.fsdglossario.fragments.AllTermsFragment;
import com.fsdmoc.fsdglossario.fragments.RecentTermsFragment;
import com.fsdmoc.fsdglossario.fragments.SearchFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);

        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_switch_language: {
                switchLanguageTemporarily();
                break;
            }
            case R.id.action_default_language: {
                showDefaultLanguageDialog();
                break;
            }
            case R.id.action_share_app: {

                logAnalyticsShareApp();
                shareApp();

                break;
            }
            case R.id.action_rate_app: {

                logAnalyticsReviewApp();
                rateApp();
                //openGooglePlay();

                break;
            }
            case R.id.action_email_suggestion: {
                emailSuggestion();
                break;
            }
            case R.id.action_about_app: {
                logAnalyticsAboutApp();
                openAboutAppActivity();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void openAboutAppActivity() {

        Intent intent = new Intent(this, AboutAppActivity.class);
        startActivity(intent);
    }

    private void shareApp() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_text));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_app_title)));
    }

    private void emailSuggestion() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:fsdmoc@fsdmoc.com"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_suggestion_email_subject));

        startActivity(Intent.createChooser(emailIntent, getString(R.string.email_suggestion_chooser_title)));
    }

    public void rateApp() {
        // you can also use BuildConfig.APPLICATION_ID
        String appId = getPackageName();
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));

        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp : otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.vending")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                // make sure it does NOT open in the stack of your activity
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task reparenting if needed
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                //  this make sure it still go to the app page you requested
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this make sure only the Google Play app is allowed to
                // intercept the intent
                rateIntent.setComponent(componentName);
                startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if Google Play not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appId));
            startActivity(webIntent);
        }
    }

    public void openGooglePlay() {

        Intent intent = new Intent();
        intent.setPackage("com.android.vending");

        startActivity(intent);
    }

    private void switchLanguageTemporarily() {

        Toast.makeText(this, R.string.toast_changed_language, Toast.LENGTH_SHORT).show();

        Settings settings = new Settings(this);
        settings.temporarilySwitchAppLanguage();

        recreate();
        Locale locale = getResources().getConfiguration().locale;
        Log.d("Language", "locale do resources e:" + locale);
    }

    private void showDefaultLanguageDialog() {

        final Settings settings = new Settings(this);
        String prefLang = settings.getPreferredLanguage();

        int dialogSelectedIndex = 0;//first is pt (depends on array resource order.)
        if (prefLang.equals(Settings.LANGUAGE_EN)) {

            dialogSelectedIndex = 1;//second is english (depends on array resource ordem)
        }

        final int preferredLangIndex = dialogSelectedIndex;

        MaterialDialog defaultLanguageDialog = new MaterialDialog.Builder(this)
                .title(R.string.dialog_default_language_title)
                .negativeText(R.string.cancel)
                .negativeColorRes(R.color.black)
                .content(R.string.dialog_default_language_content)
                .widgetColorRes(R.color.colorPrimary)
                .items(R.array.available_languages)
                .itemsCallbackSingleChoice(dialogSelectedIndex, new MaterialDialog.ListCallbackSingleChoice() {

                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int selectedIndex, CharSequence text) {

                        if (preferredLangIndex != selectedIndex) {//user changed language

                            String lang;
                            if (selectedIndex == 0)
                                lang = Settings.LANGUAGE_PT;
                            else
                                lang = Settings.LANGUAGE_EN;

                            settings.savePreferredLanguage(lang);
                            settings.applyLanguageToApp(lang);
                            recreate();
                        }

                        return true;
                    }
                })
                .show();
    }

    private void logAnalyticsShareApp() {

        FirebaseAnalytics.getInstance(this).logEvent("SHARE_APP", null);
    }

    private void logAnalyticsAboutApp() {
        FirebaseAnalytics.getInstance(this).logEvent("OPEN_ABOUT_APP", null);

    }

    private void logAnalyticsReviewApp() {
        FirebaseAnalytics.getInstance(this).logEvent("REVIEW_APP", null);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {

                return new SearchFragment();
            } else if (position == 1) {

                return new AllTermsFragment();

            } else if (position == 2) {

                return new RecentTermsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.app_section_0);
                case 1:
                    return getString(R.string.app_section_1);
                case 2:
                    return getString(R.string.app_section_2);

            }
            return null;
        }
    }


}
