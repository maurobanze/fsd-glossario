package com.fsdmoc.fsdglossario;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AboutAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        setTitle(R.string.activity_title_about_app);

    }
}
