package com.fsdmoc.fsdglossario.datastore;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

/**
 *
 */

public class Settings {

    private Activity activityContext;
    public static String ActiveLanguage;

    public Settings(Activity activityContext) {
        this.activityContext = activityContext;
    }

    //***********   FIRST RUN **************\\

    public boolean isFirstAppRun() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activityContext);
        int firstAppRun = prefs.getInt(PREFERENCE_FIRST_TIME, 0);

        return firstAppRun == 0;
    }

    public void saveNotFirstAppRun() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activityContext);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        prefsEditor.putInt(PREFERENCE_FIRST_TIME, 1);
        prefsEditor.apply();

    }

    private static final String PREFERENCE_FIRST_TIME = "PREFERENCE_FIRST_TIME";

    //***********   LANGUAGE AND LOCALE **************\\

    /**
     * Fetches system locale
     *
     * @return
     */
    private Locale getSystemLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            return activityContext.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return activityContext.getResources().getConfiguration().locale;
        }
    }

    /**
     * Applies provided language to the app UI, by modifying its configuration
     * TODO: Accomodate for Android N, which changed the way to do configuration changes
     *
     * @param lang
     */
    public void applyLanguageToApp(String lang) {
        ActiveLanguage = lang;
        Log.v("Locale", "Active Language set");

        Locale locale = new Locale(lang);
        Resources res = activityContext.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocale(locale);

        } else {
            conf.locale = locale;
            res.updateConfiguration(conf, dm);
        }

        ActiveLanguage = lang;
    }

    /**
     * Apply preferred language to UI. Fetches language from
     * SharedPreferences
     */
    public void applyPreferredLanguageToApp() {

        applyLanguageToApp(getPreferredLanguage());
    }

    /**
     * Saves language preference to sharedpreferences
     *
     * @param lang
     */
    public void savePreferredLanguage(String lang) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activityContext);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        prefsEditor.putString(PREFERENCE_LANGUAGE, lang);
        prefsEditor.apply();
    }

    /**
     * Gets preferred language from SharedPreferences
     *
     * @return
     */
    public String getPreferredLanguage() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activityContext);
        String language = prefs.getString(PREFERENCE_LANGUAGE, "bug");

        //Toast.makeText(activityContext, "preferred: " + language, Toast.LENGTH_SHORT).show();

        return language;
    }


    /**
     * Gets the system locale and sets as our own or defaults to english in case system locale isn't
     * supported. Only english and portuguese are supported
     *
     * @return : true if system language is supported. false otherwise
     */
    public boolean saveSystemLanguage() {

        Locale systemLocale = getSystemLocale();
        String language = systemLocale.getLanguage();

        //Toast.makeText(activityContext, "locale language: " + language, Toast.LENGTH_SHORT).show();

        if (language.equals(LANGUAGE_EN) || language.equals(LANGUAGE_PT)) {
            savePreferredLanguage(language);

            //Toast.makeText(activityContext, "supported. saving language", Toast.LENGTH_SHORT).show();
            return true;

        } else {//unsupported language. Default to English

            savePreferredLanguage(LANGUAGE_EN);

            //Toast.makeText(activityContext, "unsupported. saving english language", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * Changes UI without changing sharedPreferences.
     */
    public void temporarilySwitchAppLanguage() {

        if (ActiveLanguage.equals(LANGUAGE_PT)) {
            applyLanguageToApp(LANGUAGE_EN);
            Log.d("Settings", "Applied English language");

        } else {
            applyLanguageToApp(LANGUAGE_PT);
            Log.d("Settings", "Applied Portuguese language");

        }

        Log.d("Settings", "ActiveLanguage=" + ActiveLanguage);
    }

    public static final String PREFERENCE_LANGUAGE = "LANGUAGE";
    public static final String LANGUAGE_PT = "pt";
    public static final String LANGUAGE_EN = "en";

    public static final int MAX_RECENT_TERMS_PER_LANG = 10;
}
