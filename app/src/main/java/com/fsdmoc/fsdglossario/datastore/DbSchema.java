package com.fsdmoc.fsdglossario.datastore;

/**
 * Created by MauroBanze on 7/9/17.
 */

public final class DbSchema {

    private DbSchema(){

    }

    public static class Term {

        public static final String TABLE_NAME = "terms";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_EN_NAME = "enName";
        public static final String COLUMN_DEFINITION = "definition";
        public static final String COLUMN_EN_DEFINITION = "enDefinition";
        public static final String COLUMN_ADDITIONAL_INFO = "additionalInformation";
        public static final String COLUMN_EN_ADDITIONAL_INFO = "enAdditionalInformation";
        public static final String COLUMN_IMAGE_RESOURCE_NAME = "imageResourceName";
        public static final String COLUMN_EN_IMAGE_RESOURCE_NAME = "enImageResourceName";


    }

    public static class RecentTerm{

        public static final String TABLE_NAME = "recent_terms";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TERM_ID = "termId";
        public static final String COLUMN_TERM_NAME = "termName";
        public static final String COLUMN_TERM_LANGUAGE = "termLanguage";

    }
}
