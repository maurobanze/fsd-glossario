package com.fsdmoc.fsdglossario.datastore;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fsdmoc.fsdglossario.entities.RecentTerm;
import com.fsdmoc.fsdglossario.entities.Term;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Handles connection with local SQLite Database, providing public methods for accessing DB.
 * Uses a Cache internal class to hold data that needs to be readly available for use,
 * improving app speeds and avoiding multiple trips to the DB.
 * <p>
 * Also handles copying the external glossary.db produced outside the Android project. It does so
 * by being a subclass of SQLiteAssetHelper, a library.
 * <p>
 * To instantiate, don't use the constructor. Instead, use initializeOrReturn(), so only ONE instance
 * is mantained across the entire application, avoiding conflits caused by multiple open connections
 *
 */
public class LocalDatastore extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "glossary.db";
    private static final int DATABASE_VERSION = 2;

    private LocalDatastore(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();//replace db with latest if db version changes

    }

    public ArrayList<Term> getAllTerms(String language) {

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = new String[2];
        projection[0] = DbSchema.Term.COLUMN_ID;

        String columnName;

        if (language.equals(Settings.LANGUAGE_PT)) {

            columnName = DbSchema.Term.COLUMN_NAME;
        } else {

            columnName = DbSchema.Term.COLUMN_EN_NAME;
        }

        projection[1] = columnName;

        String selection = columnName + " != ?";
        String[] selectionArgs = {""};

        String orderBy = columnName + " COLLATE NOCASE";

        Cursor cursor = db.query(DbSchema.Term.TABLE_NAME, projection, selection, selectionArgs, null,
                null, orderBy);

        ArrayList<Term> terms = new ArrayList<>();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndexOrThrow(DbSchema.Term.COLUMN_ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(columnName));

            Term term = new Term(id, name);
            terms.add(term);

            Log.v("SQLITE", id + ", " + name);
        }

        cursor.close();

        Cache.terms = terms;
        return terms;
    }

    public Term getTerm(int id) {

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {DbSchema.Term.COLUMN_NAME, DbSchema.Term.COLUMN_EN_NAME,
                DbSchema.Term.COLUMN_DEFINITION, DbSchema.Term.COLUMN_EN_DEFINITION,
                DbSchema.Term.COLUMN_ADDITIONAL_INFO, DbSchema.Term.COLUMN_EN_ADDITIONAL_INFO,
                DbSchema.Term.COLUMN_IMAGE_RESOURCE_NAME, DbSchema.Term.COLUMN_EN_IMAGE_RESOURCE_NAME
        };

        String selection = DbSchema.Term.COLUMN_ID + " = ?";
        String[] selectionArgs = {id + ""};


        Cursor cursor = db.query(DbSchema.Term.TABLE_NAME, projection, selection, selectionArgs, null,
                null, null);

        boolean exists = cursor.moveToNext();

        if (exists) {

            String name = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_NAME));
            String enName = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_EN_NAME));
            String definition = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_DEFINITION));
            String enDefinition = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_EN_DEFINITION));
            String additionalInformation = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_ADDITIONAL_INFO));
            String enAdditionalInformation = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_EN_ADDITIONAL_INFO));
            String imageResourceName = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_IMAGE_RESOURCE_NAME));
            String enImageResourceName = cursor.getString(cursor.getColumnIndexOrThrow(
                    DbSchema.Term.COLUMN_EN_IMAGE_RESOURCE_NAME));

            cursor.close();

            Term term = new Term(id, name, enName, definition, enDefinition, additionalInformation,
                    enAdditionalInformation, imageResourceName, enImageResourceName);

            return term;
        }

        return null;
    }

    /**
     * Searches directly from Cache. TODO: If not available, fetch from DB
     */
    public ArrayList<Term> getMatchingTerms(String nameQuery) {

        Log.d("Datastore", "getting matching terms");

        ArrayList<Term> results = new ArrayList<>();
        for (Term term : Cache.terms) {


            if (term.getName().toLowerCase().contains(nameQuery.toLowerCase())) {

                results.add(term);
            }
        }

        Log.d("Datastore", "terms found count: " + results.size());

        return results;
    }

    public ArrayList<RecentTerm> getRecentTerms(String lang) {

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {DbSchema.RecentTerm.COLUMN_ID, DbSchema.RecentTerm.COLUMN_TERM_ID,
                DbSchema.RecentTerm.COLUMN_TERM_NAME, DbSchema.RecentTerm.COLUMN_TERM_LANGUAGE};

        String selection = DbSchema.RecentTerm.COLUMN_TERM_LANGUAGE + " = ?";
        String[] selectionArgs = {lang};

        String orderBy = DbSchema.RecentTerm.COLUMN_ID + " DESC";


        Cursor cursor = db.query(DbSchema.RecentTerm.TABLE_NAME, projection, selection, selectionArgs, null, null,
                orderBy, null);

        ArrayList<RecentTerm> recentTerms = new ArrayList<>();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(
                    DbSchema.RecentTerm.COLUMN_ID));
            int termId = cursor.getInt(cursor.getColumnIndex(
                    DbSchema.RecentTerm.COLUMN_TERM_ID));
            String name = cursor.getString(cursor.getColumnIndex(
                    DbSchema.RecentTerm.COLUMN_TERM_NAME));
            String language = cursor.getString(cursor.getColumnIndex(
                    DbSchema.RecentTerm.COLUMN_TERM_LANGUAGE));

            RecentTerm recentTerm = new RecentTerm(id, termId, name, language);
            recentTerms.add(recentTerm);
        }

        cursor.close();

        Cache.recentTerms = recentTerms;//cache recent terms in memory for later use

        if(!recentTerms.isEmpty())
            Cache.mostRecentTermIdAcrossLang = getMostRecentTermIdAcrossLanguages(db);

        return recentTerms;
    }

    private int getMostRecentTermIdAcrossLanguages(SQLiteDatabase db) {

        String SQL = "SELECT MAX(id) FROM " + DbSchema.RecentTerm.TABLE_NAME;
        Cursor cursor = db.rawQuery(SQL, null);

        boolean exists = cursor.moveToFirst();
        if (exists) {

            int maxId = cursor.getInt(0);

            Log.d("Recent Term", "Loaded max value = " + maxId);

            cursor.close();
            return maxId;

        } else {

            Log.e("Recent Term", "Couldn't load max value. Returning -1");

            cursor.close();
            return -1;
        }
    }

    public void addToRecents(Term term, String language) {

        /*
          verificar se o elemento que se pretende introduzir ja existe nos recentes. Caso sim, verificar
          se ele é o elemento mais recente (maior id). Caso sim, nao se faz nada. Caso nao,
          modifica-se o seu id para passar a ter o maior id. Caso nao exista,
           verificar se a lista esta cheia. Caso não, simplesmente acrescento um novo elemento
           na lista. Caso esteja cheia, elimino o elemento com menor id, e acrescento o novo.
         */

        if (Cache.recentTerms != null) {

            for (int i = 0; i < Cache.recentTerms.size(); i++) {//look for the term in the cache

                RecentTerm recentTerm = Cache.recentTerms.get(i);
                if (recentTerm.getTermId() == term.getId()) {//elemento já existe nos recentes

                    Log.d("Recent Item", "Found recent item in Cache");

                    if (i != 0) {// se o elemento não está na lista como o mais recente

                        Log.d("Recent Item", "Item isn't most recent. Attempting repositioning");

                        //colocar o elemento como mais recente tanto na cache como na db.

                        //db update
                        updateRecentTermId(recentTerm.getId());

                        //reproduce the previous change in the object. Here we don't increment because
                        //it has already been done in the updateRecentTermId method.
                        recentTerm.setId(Cache.mostRecentTermIdAcrossLang);

                        //reposition in array list
                        Cache.recentTerms.remove(recentTerm);
                        Cache.recentTerms.add(0, recentTerm);//add at 0, since recentTerms starts from most recent*/

                    }
                    //como o elemento ja estava na cache, nada mais precisa ser feito.
                    return;

                }
            }

            //o for loop terminou e o elemento NAO existe nos recentes. Deve se adicionar a lista
            Log.d("Recent Term", "Item not found in cache. Must be inserted as new");

            if (Cache.recentTerms.size() == Settings.MAX_RECENT_TERMS_PER_LANG) {
                // lista de termos recentes está lotada. Apagar o mais antigo

                Log.d("Recent Term", "Recent List is full. Attempting to delete oldest term before inserting");

                RecentTerm oldestTerm = Cache.recentTerms.get(Cache.recentTerms.size() - 1);
                deleteRecentTerm(oldestTerm);
            }

            Log.d("Recent Term", "Inserting new term");

            String termName;
            if (language.equals(Settings.LANGUAGE_PT))
                termName = term.getName();
            else
                termName = term.getEnName();

            RecentTerm recentTerm = new RecentTerm(-1, term.getId(), termName,
                    language);

            insertNewRecentTerm(recentTerm);

        }

    }

    /**
     * The recentTerm in question needs to have its id changed to = mostRecentTerm + 1, so it can
     * become the most recent term in the db. Cache is used because it stores the most recent term id.
     * Updates db as well as Cache
     *
     * @param toBeChangedRecentTermId: id of the recentTerm in question, that needs repositioning
     */
    private void updateRecentTermId(int toBeChangedRecentTermId) {

        SQLiteDatabase db = getWritableDatabase();

        String selection = DbSchema.RecentTerm.COLUMN_ID + " = ?";
        String[] selectionArgs = {toBeChangedRecentTermId + ""};

        ContentValues values = new ContentValues();
        values.put(DbSchema.RecentTerm.COLUMN_ID, ++Cache.mostRecentTermIdAcrossLang);//id passa a ser 1 acima do maior id

        int count = db.update(DbSchema.RecentTerm.TABLE_NAME, values, selection, selectionArgs);

        Log.d("Recent Terms", "attemped sql update recent term order id. returned row update count: " + count);
    }

    /**
     * Inserts in db and also in Cache
     *
     * @param recentTerm
     */
    private void insertNewRecentTerm(RecentTerm recentTerm) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbSchema.RecentTerm.COLUMN_TERM_ID, recentTerm.getTermId());
        values.put(DbSchema.RecentTerm.COLUMN_TERM_NAME, recentTerm.getTermName());
        values.put(DbSchema.RecentTerm.COLUMN_TERM_LANGUAGE, recentTerm.getTermLanguage());

        long insertRowId = db.insert(DbSchema.RecentTerm.TABLE_NAME, null, values);

        recentTerm.setId((int) insertRowId);

        Cache.recentTerms.add(0, recentTerm);

        Log.d("Recent Terms", "attempted insert recent term. Row id returned= " + insertRowId);
    }

    private void deleteRecentTerm(RecentTerm recentTerm) {

        SQLiteDatabase db = getWritableDatabase();

        String selection = DbSchema.RecentTerm.COLUMN_ID + " = ?";
        String[] selectionArgs = {recentTerm.getId() + ""};

        int deletedRowId = db.delete(DbSchema.RecentTerm.TABLE_NAME, selection, selectionArgs);

        Cache.recentTerms.remove(recentTerm);
        Log.d("Recent Terms", "attemped delete. Row id returned= " + deletedRowId);
    }

    /**
     * Caches data so that retrieval is fast and not reliant on constant db fetches.
     */
    private static class Cache {

        static ArrayList<Term> terms;
        static ArrayList<RecentTerm> recentTerms;
        static int mostRecentTermIdAcrossLang;
    }

    /**
     * This method should be used to create or get Datastore instances.
     * The idea is that only ONE db helper instance should be used across the entire app to prevent
     * multiple connections to the same db being created simultaneously.
     *
     * @param app: used to obtain a context reference. A direct Context object isn't asked because
     *             that is memory-leak error-prone. If by mistake an Activity context is passed,
     *             the DBHelper static reference can hold on to activities that no longer exist,
     *             creating memory-leaks.
     */
    public static LocalDatastore initializeOrReturn(Application app) {

        if (datastore == null) {

            Log.d("Datastore", "instance was unavailable. Creating new.");
            datastore = new LocalDatastore(app.getApplicationContext());
        }

        return datastore;
    }

    /**
     * Static instance used to make DBHelper class instance unique across the app.
     * It is private so it can only be accessed through @initializeOrReturn(), so it can be
     * safely instantiated when needed.
     */
    @SuppressLint("StaticFieldLeak")
    private static LocalDatastore datastore;
}
