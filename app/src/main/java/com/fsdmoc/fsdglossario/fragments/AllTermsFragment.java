package com.fsdmoc.fsdglossario.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsdmoc.fsdglossario.R;
import com.fsdmoc.fsdglossario.adapters.TermListAdapter;
import com.fsdmoc.fsdglossario.datastore.LocalDatastore;
import com.fsdmoc.fsdglossario.datastore.Settings;
import com.fsdmoc.fsdglossario.entities.Term;

import java.util.List;

/**
 *
 */
public class AllTermsFragment extends Fragment {

    public AllTermsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_terms, container, false);
        RecyclerView recyclerView = (RecyclerView) view;

        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new TermListAdapter(fetchTermsFromDb()));

        return view;
    }

    private List<Term> fetchTermsFromDb() {

        LocalDatastore db = LocalDatastore.initializeOrReturn(getActivity().getApplication());

        return db.getAllTerms(Settings.ActiveLanguage);
        //TODO: Get the constant from preferences/settings
    }


}
