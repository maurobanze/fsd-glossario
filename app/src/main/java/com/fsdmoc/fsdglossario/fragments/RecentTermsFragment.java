package com.fsdmoc.fsdglossario.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsdmoc.fsdglossario.R;
import com.fsdmoc.fsdglossario.adapters.RecentTermListAdapter;
import com.fsdmoc.fsdglossario.datastore.LocalDatastore;
import com.fsdmoc.fsdglossario.datastore.Settings;
import com.fsdmoc.fsdglossario.entities.RecentTerm;

import java.util.ArrayList;


public class RecentTermsFragment extends Fragment {


    private RecyclerView recyclerView;

    public RecentTermsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_recent_terms, container, false);
        recyclerView = (RecyclerView) view;

        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        recyclerView.setAdapter(new RecentTermListAdapter(fetchRecentTermsFromDb()));

    }

    private ArrayList<RecentTerm> fetchRecentTermsFromDb() {

        LocalDatastore db = LocalDatastore.initializeOrReturn(getActivity().getApplication());

        return db.getRecentTerms(Settings.ActiveLanguage);
    }

}
