package com.fsdmoc.fsdglossario.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.fsdmoc.fsdglossario.R;
import com.fsdmoc.fsdglossario.adapters.TermListAdapter;
import com.fsdmoc.fsdglossario.datastore.LocalDatastore;
import com.fsdmoc.fsdglossario.entities.Term;

import java.util.ArrayList;


/**
 *
 */
public class SearchFragment extends Fragment implements SearchBoxListener {

    private RecyclerView recyclerViewResults;

    public SearchFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        initSearchBox(rootView);
        searchBoxListener = this;

        recyclerViewResults = (RecyclerView) rootView.findViewById(R.id.recyclerViewResults);
        recyclerViewResults.setLayoutManager(new LinearLayoutManager(getContext()));


        return rootView;
    }

    /**
     * SearchBox LISTENER/CLIENT methods
     **/

    @Override
    public void onSearchAction(String currentQuery) {


    }

    @Override
    public void onTextChanged(String oldQuery, String currentQuery) {

        Log.d("Datastore", "");

        if (currentQuery.length() != 0) {

            //fazer query na base de dados e interagir com o recyclerview mostrando results
            ArrayList<Term> matchingTerms = getMatchingTerms(currentQuery);
            updateRecyclerView(matchingTerms);

        } else {
            recyclerViewResults.setAdapter(null);
        }

    }

    private void updateRecyclerView(ArrayList<Term> matchingTerms) {

        recyclerViewResults.setAdapter(new TermListAdapter(matchingTerms));

    }

    private ArrayList<Term> getMatchingTerms(String query) {

        return LocalDatastore.initializeOrReturn(getActivity().getApplication()).getMatchingTerms(query);
    }

    /**
     * SearchBox WORKER methods. Should be in their own class
     **/


    private ImageView imageViewNav, imageViewErase;
    private EditText editTextSearchTerm;

    private void initSearchBox(View rootView) {

        ViewGroup searchBox = (ViewGroup) rootView.findViewById(R.id.searchBox);
        imageViewNav = (ImageView) searchBox.findViewById(R.id.imageViewNav);
        imageViewErase = (ImageView) searchBox.findViewById(R.id.imageViewErase);
        editTextSearchTerm = (EditText) searchBox.findViewById(R.id.editTextSearchTerm);

        imageViewNav.setOnClickListener(searchBoxClickListener);
        imageViewErase.setOnClickListener(searchBoxClickListener);
        editTextSearchTerm.addTextChangedListener(textWatcher);
    }

    private View.OnClickListener searchBoxClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            int id = view.getId();
            if (id == R.id.imageViewNav) {


            } else if (id == R.id.imageViewErase) {

                editTextSearchTerm.setText("");
            }

        }
    };
    private TextWatcher textWatcher = new TextWatcher() {

        private String oldText;

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            this.oldText = charSequence.toString();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            int length = charSequence.length();
            if (length == 0) {
                setEraseButtonVisible(false);

            } else {//Test for performance
                setEraseButtonVisible(true);
            }

            searchBoxListener.onTextChanged(oldText, charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void setEraseButtonVisible(boolean visible) {

        if (visible == true) {
            imageViewErase.setVisibility(View.VISIBLE);
        } else {
            imageViewErase.setVisibility(View.INVISIBLE);
        }
    }

    SearchBoxListener searchBoxListener;

    /** -- END of SearchBox WORKER Methods-- **/

    /**
     * Search Results RecyclerView Adapter methods
     */

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}


interface SearchBoxListener {

    void onSearchAction(String currentQuery);

    void onTextChanged(String oldQuery, String currentQuery);

}