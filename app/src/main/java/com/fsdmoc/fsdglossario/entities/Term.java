package com.fsdmoc.fsdglossario.entities;



/**
 * Represents a term such as "banca móvel"
 */

public class Term {

    private int id;

    private String name;
    private String enName;

    private String definition;
    private String enDefinition;

    private String additionalInformation;
    private String enAdditionalInformation;

    private String imageResourceName;
    private String enImageResourceName;

    public Term() {

    }

    public Term(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Term(int id, String name, String definition, String additionalInformation) {
        this.id = id;
        this.name = name;
        this.definition = definition;
        this.additionalInformation = additionalInformation;
    }

    public Term(int id, String name, String definition, String additionalInformation, String imageResourceName) {
        this.id = id;
        this.name = name;
        this.definition = definition;
        this.additionalInformation = additionalInformation;
        this.imageResourceName = imageResourceName;
    }

    public Term(int id, String name, String enName, String definition, String enDefinition,
                String additionalInformation, String enAdditionalInformation, String imageResourceName,
                String enImageResourceName) {
        this.id = id;
        this.name = name;
        this.enName = enName;
        this.definition = definition;
        this.enDefinition = enDefinition;
        this.additionalInformation = additionalInformation;
        this.enAdditionalInformation = enAdditionalInformation;
        this.imageResourceName = imageResourceName;
        this.enImageResourceName = enImageResourceName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getEnDefinition() {
        return enDefinition;
    }

    public void setEnDefinition(String enDefinition) {
        this.enDefinition = enDefinition;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getEnAdditionalInformation() {
        return enAdditionalInformation;
    }

    public void setEnAdditionalInformation(String enAdditionalInformation) {
        this.enAdditionalInformation = enAdditionalInformation;
    }

    public String getImageResourceName() {
        return imageResourceName;
    }

    public void setImageResourceName(String imageResourceName) {
        this.imageResourceName = imageResourceName;
    }

    public String getEnImageResourceName() {
        return enImageResourceName;
    }

    public void setEnImageResourceName(String enImageResourceName) {
        this.enImageResourceName = enImageResourceName;
    }
}
