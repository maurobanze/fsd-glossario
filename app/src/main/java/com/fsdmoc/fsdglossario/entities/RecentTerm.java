package com.fsdmoc.fsdglossario.entities;

/**
 * Created by MauroBanze on 7/14/17.
 */

public class RecentTerm {

    private int id;
    private int termId;
    private String termName;
    private String termLanguage;

    public RecentTerm() {
    }

    public RecentTerm(int id, int termId, String termName, String termLanguage) {
        this.id = id;
        this.termId = termId;
        this.termName = termName;
        this.termLanguage = termLanguage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTermId() {
        return termId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getTermLanguage() {
        return termLanguage;
    }

    public void setTermLanguage(String termLanguage) {
        this.termLanguage = termLanguage;
    }
}
