package com.fsdmoc.fsdglossario.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fsdmoc.fsdglossario.R;
import com.fsdmoc.fsdglossario.TermDetailsActivity;
import com.fsdmoc.fsdglossario.entities.RecentTerm;

import java.util.List;

/**
 *
 */
public class RecentTermListAdapter extends RecyclerView.Adapter<RecentTermListAdapter.ViewHolder> {

    private final List<RecentTerm> mTerms;


    public RecentTermListAdapter(List<RecentTerm> items) {
        mTerms = items;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_term, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mTerms.get(position);
        holder.textViewTermName.setText(mTerms.get(position).getTermName());

    }

    @Override
    public int getItemCount() {
        return mTerms.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RecentTerm mItem;
        final View mView;
        final TextView textViewTermName;

        ViewHolder(View view) {
            super(view);

            mView = view;
            textViewTermName = (TextView) view.findViewById(R.id.content);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), TermDetailsActivity.class);
                    intent.putExtra(TermDetailsActivity.EXTRA_TERM_ID, mItem.getTermId());

                    view.getContext().startActivity(intent);
                }
            });
        }

    }
}
